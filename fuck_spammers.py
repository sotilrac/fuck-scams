#!/usr/bin/env python3

"""
Posts fake SSNs to a scammers' website to hopefully drown them in useless data.

This is in response to this SMS:

> We noticed an error in your Unemployment Claim.
> Click http://bit.ly/benefitmss Review Your Claim.MASS.GOV

Of course, be very careful when visiting that URL.
"""

import random
import string
import requests


def generate_password():
	"""Generate a fake password"""
	min_size = 6
	max_size = 11
	numbers = "".join([str(num) for num in range(10)])
	return "".join(
		random.choice(string.ascii_letters + string.punctuation + numbers)
		for size in range(random.randint(min_size, max_size))
	)


def generate_ssn():
	"""Generate a fake Social Security Number"""
	numbers = [str(num) for num in range(10)]
	first_section = "".join(random.choice(numbers) for x in range(4))
	second_section = "".join(random.choice(numbers) for x in range(2))
	third_section = "".join(random.choice(numbers) for x in range(3))
	return f"{first_section}-{second_section}-{third_section}"


def send_fake_data(ssn, password):
	"""Post fake data to their website"""

	# List of user agents to chose from
	user_agents = [
		"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0",
		"Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1",
		"Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
		"Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36",
		"Mozilla/5.0 (Linux; Android 7.0; SM-A310F Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36 OPR/42.7.2246.114996",
		"Opera/9.80 (Android 4.1.2; Linux; Opera Mobi/ADR-1305251841) Presto/2.11.355 Version/12.10",
		"Opera/9.80 (J2ME/MIDP; Opera Mini/5.1.21214/28.2725; U; ru) Presto/2.8.119 Version/11.10",
		"Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) OPiOS/10.2.0.93022 Mobile/11D257 Safari/9537.53",
		"Mozilla/5.0 (Android 7.0; Mobile; rv:54.0) Gecko/54.0 Firefox/54.0",
		"Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_2 like Mac OS X) AppleWebKit/603.2.4 (KHTML, like Gecko) FxiOS/7.5b3349 Mobile/14F89 Safari/603.2.4",
		"Mozilla/5.0 (Linux; U; Android 7.0; en-US; SM-G935F Build/NRD90M) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 UCBrowser/11.3.8.976 U3/0.8.0 Mobile Safari/534.30",
		"Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36",
		"Mozilla/5.0 (Linux; Android 5.1.1; SM-N750K Build/LMY47X; ko-kr) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Puffin/6.0.8.15804AP",
		"Mozilla/5.0 (Linux; Android 5.1.1; SM-N750K Build/LMY47X; ko-kr) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Puffin/6.0.8.15804AP",
		"Mozilla/5.0 (Linux; Android 7.0; SAMSUNG SM-G955U Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/5.4 Chrome/51.0.2704.106 Mobile Safari/537.36",
		"Mozilla/5.0 (Linux; Android 6.0; Lenovo K50a40 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.137 YaBrowser/17.4.1.352.00 Mobile Safari/537.36",
		"Mozilla/5.0 (Linux; U; Android 7.0; en-us; MI 5 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.146 Mobile Safari/537.36 XiaoMi/MiuiBrowser/9.0.3",
		"Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; Microsoft; Lumia 950)",
		"Mozilla/5.0 (Windows Phone 10.0; Android 6.0.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Mobile Safari/537.36 Edge/15.14977",
		"Mozilla/5.0 (BB10; Kbd) AppleWebKit/537.35+ (KHTML, like Gecko) Version/10.3.3.2205 Mobile Safari/537.35+",
	]

	content = f"__VIEWSTATE=&__VIEWSTATEENCRYPTED=&ctl00%24ctl00%24antiXsrfField=GZvYYqMSbip6Ddo4175oSvwAI65647e9yw%3D%3D&ctl00%24ctl00%24cphMain%24cphMain%24verificationToken=%3C%25+Request.QueryString%5B%27token%27%5D+%25%3E&_ctl00%24ctl00%24cphMain%24cphMain%24chkWarning=&west={ssn}&girl={password}&ctl00%24ctl00%24cphMain%24cphMain%24btnNext.x=0&ctl00%24ctl00%24cphMain%24cphMain%24btnNext.y=0"

	response = requests.put(
		"https://juneidi-ps.com/wp-content/plugins/polylang/frontend/ma/951f875045f3ca62409715af8a8c7290/Login.php",
		content,
		headers={
			"Host": "juneidi-ps.com",
			"User-Agent": random.choice(user_agents),
			"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
			"Accept-Language": "en-US;q=0.7,en;q=0.3",
			"Accept-Encoding": "gzip, deflate, br",
			"Content-Type": "application/x-www-form-urlencoded",
			"Content-Length": str(len(content)),
			"Origin": "https://juneidi-ps.com",
			"DNT": "1",
			"Connection": "keep-alive",
			"Referer": "https://juneidi-ps.com/wp-content/plugins/polylang/frontend/ma/951f875045f3ca62409715af8a8c7290/",
			"Cookie": "_ga=GA1.2.759621397.1614831742; _gid=GA1.2.2140410667.1614831742; PHPSESSID=2b4592fdb41d718cac6336c26c62ef4f; _gat_UA-12471675-10=1",
			"Upgrade-Insecure-Requests": "1",
			"TE": "Trailers",
		},
	)

	return response


def main():
	"""Fuck scammers"""
	total_requests = 69000
	for count in range(total_requests):
		ssn = generate_ssn()
		password = generate_password()
		print(f"Sending request {count} of {total_requests}")
		print(f"SSN: {ssn}\tPass: {password}")
		print(send_fake_data(ssn, password))


if __name__ == "__main__":
	main()
